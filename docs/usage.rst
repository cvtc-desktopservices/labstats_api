Usage
=====

LabStats API is designed to be a simple way to get to the data you need in LabStats. It takes away all of the hard work of interacting with the API, handling authentication, and working with pages.

Getting started
---------------

To use LabStats API, we'll start by instantiating a LabStats object::

    from os import environ
    from labstats_api import LabStats

    labstats = LabStats(api_url, api_key)

``api_url`` is your LabStats URL, such as ``https://api.labstats.com``, and ``api_key`` is your API token. You can create one in "Admin -> External Systems" in LabStats.

Now that we have the LabStats object, we can make calls as specified on the page :doc:`api/labstats`. For example::

    apps = labstats.get_apps()

    for app in apps:
        print(app.name)

    >>> 'Microsoft Word'
    >>> 'Microsoft Excel'

You can pass arbitrary parameters to any method as keyword arguments and they will be added to your query. For more information, see `Keyword arguments`_.

Working with LabStats Objects
-----------------------------

Whenever you create a query with LabStats API, you will receive either a single object which is a subclass of ``LabStatsObject`` or an ``AfterIDPaginatedList`` which contains ``LabStatsObject``'s. This makes accessing data easy.

LabStatsObject
^^^^^^^^^^^^^^

LabStats API converts JSON objects from the LabStats REST API into Python objects. This means that you can access the data of an object without hassling with ``json.loads()`` or ``object["attribute"]``.

For example, ``Applications`` in the REST API have a ``vendor`` attribute. So, once we have an ``Application``, we can access this attribute::

    app.vendor
    >>> Microsoft Corporation

We've attempted to document all of these attributes in :doc:`api/index`, but the REST API may change without notice. In this case, you can simply use the new attribute with no changes to LabStats API. For example, if a new ``color`` attribute was added to ``Applications``, ``Application.color`` would instantly be available to your code.

AfterIDPaginatedList
^^^^^^^^^^^^^^^^^^^^

The :class:`labstats.paginated_list.AfterIDPaginatedList` is an abstraction of the LabStats REST API's approach to serving multiple pages of results. When it runs out of locally loaded results, it will load more with another request to the API. For most purposes, you can treat it as a regular list::

    apps = labstats.get_apps()
    apps[0].name

    >>> 'Microsoft Word'

    apps[1].name

    >>> 'Microsoft Excel'

The list may also be sliced::

    slice = apps[0:10]
    for app in slice:
        print(app.name)

    >>> Microsoft Word
    >>> Microsoft Excel
    >>> Microsoft PowerPoint
    >>> Microsoft Outlook
    >>> Microsoft Publisher
    >>> Microsoft Internet Explorer
    >>> Mozilla Firefox
    >>> Google Chrome
    >>> Notepad++
    >>> Notepad

Keyword arguments
-----------------

Almost all calls to LabStats API create a query to the LabStats REST API. These queries can take a number of parameters which are specified in the `LabStats REST API documentation <https://api.labstats.com/index.html>`_. In every case, we have attempted to document these parameters in the :doc:`api/index`, but the REST API is subject to change with no (or little) notice. If you see a parameter that you want to add to your query but you don't see it in the API reference, you can add it as an arbitrary keyword argument.

For example, suppose that LabStats added a ``color`` parameter to the ``/apps`` endpoint. You notice this on the `LabStats REST API Documentation`_ and would like to filter your searches to applications which are blue. To do this::

    labstats.get_applications(color="blue")

"Gotchas"
---------

These are interesting or potentially surprising things you may encounter while using LabStats API.

.. _limit:

Limit
^^^^^

Many API calls have a ``limit`` parameter which sets the number of items to request per page returned. We've tried to stick with the default values set by LabStats when dealing with these values, but you may want to tune them.

If you will only ever use a few items of a query, it does not make sense to ask your LabStats instance for more. You'll slightly reduce your bandwidth and memory requirements by setting a lower value for the limit and staying under that value when using the resulting list. If you go over your limit in the resulting list, you'll end up making a new request to the REST API which will take a small amount of time.

For example, setting a limit of 25 and using items 0-24 of the resulting list is optimal. A request for item 25 will cause the list to load more items. Requesting a very high item, such as item 300, will cause the list to load multiple pages in an attempt to get to your requested item. In this case it is better to set a higher limit (or try to filter using other parameters in the query so you don't need to request so many items).

Results do not update automatically
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you request an item using LabStats API then change the item's data in LabStats, this change will not be reflected in your local object. You will need to run your original query again to get updated data. Luckily, most calls in LabStats API accept either an integer ID or the same objects that they return as parameters to get new data. For example::

    app = labstats.get_app(1000)
    app.name

    >>> Microsoft Word

    # Change a few things using the LabStats UI...
    app.name

    >>> Microsoft Word

    app = labstats.get_app(app)
    app.name

    >>> Neat Word Processing

Similarly, lists of results may be changed on the LabStats instance between the time it is first requested and when you ask for more data. To reduce the risk of this causing issues for you, set your `Limit` to an appropriate value and request as many results as you need quickly after requesting the list. For example, if you will use an entire list of applications, request and then fill the list immediately::

    apps = labstats.get_applications()
    [__ for __ in apps]

This list comprehension iterates through every value in the list and does nothing with it, filling the list. Whenever the list is used after this point, each result is pulled from memory rather than from the REST API.

.. _after_id:

The After ID
^^^^^^^^^^^^

Calls which return an `AfterIDPaginatedList`_ take an ``after_id`` argument. This argument is used in the REST API's pagination, but you can also use it in limited cases. The ``after_id`` says "I would like all of the objects which have an ID greater than this one." In other words, if you pass ``1001``, you'll get objects with ID ``1002`` and up. If you don't have an object with ID ``1002``, you'll receive the next smallest ID after ``1001``.
