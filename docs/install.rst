Install
=======

LabStats API requires Python 3.6 or higher.

You can install LabStats API...

Via pipenv::

    pipenv install git+https://bitbucket.org/cvtc-desktopservices/labstats_api.git#egg=labstats_api

Via pip::

    pip install git+https://bitbucket.org/cvtc-desktopservices/labstats_api.git#egg=labstats_api
