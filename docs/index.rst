Welcome to labstats_api's documentation!
========================================

LabStats API is an unofficial Python wrapper for the `LabStats REST API <https://api.labstats.com>`_. It can be used to quickly and easily analyze data from LabStats.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   usage
   api/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
