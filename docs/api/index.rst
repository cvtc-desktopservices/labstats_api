API Reference
=============

This section contains reference material for LabStats API. Note that the documentation for this Python wrapper may fall behind the real API, so it's best to also have `the LabStats REST API Documentation <https://api.labstats.com/index.html>`_ open for reference.

If you'd like a prose introduction to LabStats API, see :doc:`../usage`. Otherwise, :doc:`labstats` is probably where you want to start.

.. toctree::
   :maxdepth: 1
   :caption: API

   labstats
   models
   api_errors
   internal
