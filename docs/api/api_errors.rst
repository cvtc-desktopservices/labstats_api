API Errors
----------

.. automodule:: labstats_api.api_errors
   :members:
   :undoc-members:
   :show-inheritance:
