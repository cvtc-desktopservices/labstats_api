Internal use
============

These objects are meant only for internal use by LabStats API but their documentation is included here for completeness (or if you want to contribute)

.. autoclass:: labstats_api.models.LabStatsObject
   :members:
   :undoc-members:

.. automodule:: labstats_api.paginated_list
   :members:
   :undoc-members:

.. automodule:: labstats_api.requester
   :members:
   :undoc-members:
