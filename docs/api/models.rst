Models
======

Application
-----------

.. autoclass:: labstats_api.models.Application
   :members:
   :show-inheritance:

ApplicationVersion
------------------

.. autoclass:: labstats_api.models.ApplicationVersion
   :members:
   :show-inheritance:

Group
-----

.. autoclass:: labstats_api.models.Group
   :members:
   :show-inheritance:


GroupStatus
-----------

.. autoclass:: labstats_api.models.GroupStatus

Station
-------

.. autoclass:: labstats_api.models.Station
   :members:
   :show-inheritance:

StationMetadataAttribute
------------------------

.. autoclass:: labstats_api.models.StationMetadataAttribute

StationStatus
-------------

.. autoclass:: labstats_api.models.StationStatus
   :members:

StationTagGroup
---------------

.. autoclass:: labstats_api.models.StationTagGroup
   :members:
