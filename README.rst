labstats_api
============

**Watch out!**

LabStats API is unmaintained.

Description
-----------

LabStats API is an unofficial Python wrapper for the `LabStats REST API <https://api.labstats.com>`_::

    import labstats_api

    labstats = labstats_api.LabStats({Your API URL}, {Your API key})
    labstats.get_app(1000)
    >>> Application(_requester=<[...]>, id=1000, name=Microsoft Word, vendor=Microsoft Corporation, source=catalog, description=None, type=desktop, is_tracked=True, tracking_pattern_match=wildcard)

You can find the documentation at https://labstats-api.readthedocs.io/en/latest/

Install
-------

LabStats API requires Python 3.6 or higher.

You can install LabStats API...

Via pipenv::

    pipenv install git+https://bitbucket.org/cvtc-desktopservices/labstats_api.git

Via pip::

    pip install git+https://bitbucket.org/cvtc-desktopservices/labstats_api.git

Supported endpoints
-------------------

LabStats API currently supports the following endpoints:

* Applications
    * ``/apps``
    * ``/apps/{id}``
    * ``/apps/{id}/versions``
    * ``/apps/tags``
* Groups
    * ``/groups``
    * ``/groups/{id}``
    * ``/groups/{id}/groups``
    * ``/groups/{id}/status``
* Stations
    * ``/stations``
    * ``/stations/{id}``
    * ``/stations/{id}/metadata``
    * ``/stations/{id}/apps``
    * ``/stations/{id}/status``
    * ``/stations/tag_groups``
    * ``/stations/tags_groups/{tag_group}/tags``
    * ``/stations/tags``
    * ``/stations/tags/{tag}/stations``


Develop
-------

To get started developing on LabStats API, you'll need ``pipenv`` installed. Clone the repository then run the following command in the cloned directory::

    pipenv install --dev

All files must be formatted with Black prior to testing or committing. Black runs on Python 3.6 and up. You will need to install it as a system-wide dependency by running ``pip install black --user``. Then, you can reformat all of your files with::

    black labstats_api/
    black tests/

You can run the unit tests with the following command. Note that this also runs pylint on the codebase::

    pipenv run pytest --pylint --pylint-rcfile=.pylintrc

You can also build the documentation locally::

    cd docs/
    pipenv run make html

The built documentation will then be in ``_build/html/`` inside the ``docs/`` directory. The documentation will fail to build if there are any warnings in your ReStructuredText syntax in either your ``.rst`` files or docstrings.

License
-------

LabStats API is licensed under the MIT License. For more information, see the LICENSE file in this repository.
