# Copyright (c) 2017 University of Central Florida - Center for Distributed Learning
# Copyright (c) 2019 Chippewa Valley Technical College
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Abstracts away the pagination of the LabStats API"""

# The Slice accessing a protected member of List is acceptable
# AfterIDPaginatedList.__init__() has many arguments
# pylint: disable=protected-access,too-many-arguments


class AfterIDPaginatedList:
    """
    Abstracts the ``after_id`` type pagination in LabStats, such as that used on
    ``/apps``

    :param content_class: The type of object that this list will contain. All
                          data received from LabStats will be used to create
                          this type of object.

    :type content_class: :class:`labstats_api.models.LabStatsObject`

    :param requester: Requester instance to use to get data into this list

    :type requester: :class:`labstats_api.requester.Requester`

    :param request_endpoint: The LabStats API endpoint to get data from. Omit
                             the leading slash. Add a trailing slash. For
                             example, ``apps/``

    :type request_endpoint: str

    :param request_method: ``GET`` is the only supported method for now

    :type request_method: str

    :param first_id: The exclusive ID of the minimum element that should go in
                     the list. LabStats returns data starting at the nearest ID
                     larger than it.

    :type first_id: int

    Additional parameters to the request can be passed as ``kwargs``.
    """

    def __init__(
        self,
        content_class,
        requester,
        request_endpoint,
        request_method="GET",
        first_id=1,
        **kwargs
    ):

        self._elements = list()

        self._requester = requester
        self._content_class = content_class
        self._params = kwargs or {}
        self._params["limit"] = kwargs.get("limit", 100)
        self._endpoint = request_endpoint
        self._next_id = first_id
        self._request_method = request_method

    def __getitem__(self, index):
        assert isinstance(index, (int, slice))
        if isinstance(index, int):
            if index < 0:
                raise IndexError("Cannot negative index a PaginatedList")
            self._get_up_to_index(index)
            return self._elements[index]

        return self._Slice(self, index)

    def __iter__(self):
        for element in self._elements:
            yield element
        while self._has_next():
            new_elements = self._grow()
            for element in new_elements:
                yield element

    def __repr__(self):
        return "<PaginatedList of type {}>".format(self._content_class.__name__)

    def _is_larger_than(self, index):
        return len(self._elements) > index or self._has_next()

    def _get_up_to_index(self, index):
        while len(self._elements) <= index and self._has_next():
            self._grow()

    def _grow(self):
        new_elements = self._get_next_page()
        self._elements += new_elements
        return new_elements

    def _has_next(self):
        return self._next_id is not None

    def _get_next_page(self):
        parameters = {"after_id": self._next_id}
        parameters.update(self._params)
        data = self._requester.request_data(
            self._request_method, self._endpoint, parameters=parameters
        )

        self._next_id = data.get("last_id", None)
        content = []

        for element in data["results"]:
            if element is not None:
                content.append(self._content_class(self._requester, element))

        return content

    class _Slice:
        def __init__(self, the_list, the_slice):
            self._list = the_list
            self._start = the_slice.start or 0
            self._stop = the_slice.stop
            self._step = the_slice.step or 1

            if self._start < 0 or self._stop < 0:
                raise IndexError("Cannot negative index a PaginatedList slice")

        def __iter__(self):
            index = self._start
            while not self._finished(index):
                if self._list._is_larger_than(index):
                    try:
                        yield self._list[index]
                    except IndexError:
                        return
                    index += self._step
                else:
                    return

        def _finished(self, index):
            return self._stop is not None and index >= self._stop
