""" A Python wrapper for the LabStats API"""

from .labstats import LabStats

__all__ = ["LabStats"]

__version__ = "0.1.0"
