"""Abstractions for communication with the LabStats REST API"""

import json

from .api_errors import (
    UnauthorizedError,
    RequestInvalidError,
    NotFoundError,
    LabStatsAPIError,
)


class Requester:
    """Abstracts communication with the LabStats REST API.

    :param api_url: URL to LabStats API starting with ``https://``

    :type api_url: str

    :param api_key: API authorization key to query the LabStats server with. See
                    `API Key Creation
                    <https://support.labstats.com/article/api/#api-key-creation>`_ for
                    more information.

    :type api_key: str

    :param session: A Requests Session object to use for this instance. See `Advanced
                    Usage -- Requests
                    <https://2.python-requests.org/en/master/user/advanced/>`_ for more
                    information.

    :param timeout: A Requests-compatible timeout value to be used on all requests. See
                    `Timeouts -- Requests
                    <https://2.python-requests.org/en/master/user/advanced/#timeouts>`_
                    for more information.

    :type timeout: int or tuple
    """

    def __init__(self, api_url, api_key, session, timeout=10):
        if not api_url.startswith("https://"):
            raise ValueError(
                "Your API URL must be an HTTPS URL and must start with https://"
            )

        if not api_url.endswith("/"):
            api_url += "/"

        self._api_key = api_key
        self._session = session
        self.api_url = api_url
        self.timeout = timeout

    @classmethod
    def _get_friendly_error_message(cls, error_json, status_code=""):
        """Returns a friendly error message based on an error's JSON

        Example: LabStats said: "Unauthorized request; Unauthorized request. Please
        check the permissions on your API key."
        """

        try:
            error = json.loads(error_json)["error"]
        except (TypeError, KeyError):
            error = {}

        try:
            return (
                "LabStats said: "
                + status_code
                + ' "'
                + error.get("message", "no message")
                + "; "
                + error.get("description", "no description")
                + '"'
            )
        except Exception:  # pylint: disable=broad-except
            return (
                'LabStats returned an error with status code "'
                + status_code
                + '" but I could not parse it for more information.'
            )

    @classmethod
    def _raise_for_status(cls, response):
        """Raises a relevant Exception if the status code is not 200 OK"""

        status = response.status_code

        if status == 401:
            raise UnauthorizedError(cls._get_friendly_error_message(response.text))
        if status == 400:
            raise RequestInvalidError(cls._get_friendly_error_message(response.text))
        if status == 404:
            raise NotFoundError(cls._get_friendly_error_message(response.text))
        if status != 200:
            raise LabStatsAPIError(cls._get_friendly_error_message(response.text))

    @property
    def _default_headers(self) -> dict:
        return {"Authorization": self._api_key}

    def request_data(self, method, endpoint, parameters=None, headers=None):
        """Request data from the LabStats API

        :param method: The method to use. Only ``"GET"`` is implemented

        :type method: str

        :param endpoint: The endpoint URL in LabStats, such as ``"apps/"``. Do
                         not give a leading slash. Give a trailing slash.

        :type endpoint: str

        :param parameters: URL parameters

        :type parameters: dict

        :param headers: Extra HTTP headers to send with the request

        :type headers: dict
        """

        use_headers = self._default_headers
        use_headers.update(headers if headers else {})

        api_url = self.api_url + endpoint

        if method == "GET":
            response = self._session.get(
                api_url, params=parameters, headers=use_headers, timeout=self.timeout
            )
            self._raise_for_status(response)

            return response.json()

        raise NotImplementedError("GET is the only supported method")
