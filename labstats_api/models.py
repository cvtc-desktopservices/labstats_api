# Copyright (c) 2017 University of Central Florida - Center for Distributed Learning
# Copyright (c) 2019 Chippewa Valley Technical College
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Objects which represent data in LabStats"""

import json
import re
from collections import namedtuple
from datetime import datetime
from enum import Enum
from typing import List

from .paginated_list import AfterIDPaginatedList

DATE_PATTERN = re.compile("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}")


class LabStatsObject:
    """
    Base class for all classes representing objects returned by the API.
    This makes a call to
    :func:`labstats_api.models.LabStatsObject.set_attributes`
    to dynamically construct this object's attributes with a JSON object.

    :param requester: The requester to pass HTTP requests through.

    :type requester: :class:`labstats_api.requester.Requester`

    :param attributes: The JSON object to build this object with.

    :type attributes: dict
    """

    def __init__(self, requester, attributes):
        self._requester = requester
        self.set_attributes(attributes)

    def __repr__(self):  # pragma: no cover
        classname = self.__class__.__name__
        attrs = ", ".join(
            [
                "{}={}".format(attr, val)
                for attr, val in self.__dict__.items()
                if attr != "attributes"
            ]
        )
        return "{}({})".format(classname, attrs)

    def to_json(self):
        """Return the original JSON that was used to construct the object"""

        return json.dumps(self.attributes)

    def set_attributes(self, attributes):
        """
        Load this object with attributes.
        This method attempts to detect special types based on the field's
        content and will create an additional attribute of that type.

        Consider a JSON response with the following fields::

            {
                "id": 1000
                "start_time": "2019-06-20T11:04:36.967"
                "name": "Microsoft Word"
            }

        The ``start_time`` field matches a date in RFC3339 format, so an
        additional datetime attribute is created, ``start_time_date``.
        ``start_time`` will hold the original string representing the date.
        ``start_time_date`` contains a datetime object representing the date.

        :param attributes: The JSON object to build this object with.

        :type attributes: dict
        """
        self.attributes = attributes

        for attribute, value in attributes.items():
            self.__setattr__(attribute, value)

            # datetime field
            if DATE_PATTERN.match(str(value)):
                # LabStats does not give timezone data by design. All values are given
                # in local time but there's no way to know what local time is.
                naive = datetime.strptime(value, "%Y-%m-%dT%H:%M:%S.%f")
                self.__setattr__(attribute + "_date", naive)


class Application(LabStatsObject):
    """Represents a LabStats application"""

    id: int  #: Integer unique identifier

    name: str  #: Human-readable name of the Application, such as ``"Notepad"``

    vendor: str  #: Human-readable name of the Application's vendor

    source: str
    """
    ``"catalog"``, ``"inventory"``, or ``"user_defined"``. Used as ``type``
    parameter for the ``/apps`` endpoint.
    """

    description: str  #: User-defined Application description

    type: str  #: ``"desktop"`` or ``"webapp"``

    is_tracked: bool  #: Whether LabStats is tracking usage of this Application

    tracking_pattern_match: str  #: ``"wildcard"`` or ``"reg_ex"``

    @property
    def endpoint(self):
        """The LabStats REST API endpoint that represents this object"""
        return "apps/{:d}/".format(self.id)

    def get_versions(self, limit=300, after_id=1, **kwargs):
        """Gets a list of detected versions for this application

        :param limit: See :ref:`limit`

        :param after_id: See :ref:`after_id`
        """

        kwargs.update({"limit": limit})

        return AfterIDPaginatedList(
            ApplicationVersion,
            self._requester,
            self.endpoint + "versions/",
            first_id=after_id,
            **kwargs
        )


class ApplicationVersion(LabStatsObject):
    """Represents a single version of an Application tracked by LabStats"""

    id: int  #: Integer unique identifier

    name: str  #: Human-readable name of the application at this version

    version: str  #: The application's version string, such as ``1.0.0``

    operating_system: str  #: The platform that this version was detected on

    install_count: int
    """The number of computers that this version was detected on"""

    application_id: int
    """The ID of the Application which this Version pertains to"""


class Group(LabStatsObject):
    """Represents a group of Stations or Groups"""

    id: int  #: Integer unique identifier

    name: str  #: Human-readable group name

    parent_group_id: int  #: Identifier of the group containing this group

    contents: str  #: ``"stations"`` or ``"groups"``

    description: str or None  #: Optional longer description

    schedule_id: int  #: Identifier of the Schedule assigned to this Group

    has_lab_features_enabled: bool  #: Whether Lab Features are enabled

    @property
    def endpoint(self):
        """The LabStats REST API endpoint that represents this object"""
        return "groups/{:d}/".format(self.id)

    def get_stations(self, status=None, limit=200, after_id=1, **kwargs):
        """Returns all Stations inside this group.

        If this group contains groups, returns the status of all stations within
        the groups within this group. Recursively.

        :param status: Filter based on the state of the machine. Valid values
                       are ``"powered_on"``, ``"offline"``, ``"in_use"``, or
                       ``None``.

        :param limit: See :ref:`limit`

        :param after_id: See :ref:`after_id`

        :rtype: list of :class:`Station`

        :calls: ``/groups/{id}/stations``
        """

        kwargs.update({"status": status, "limit": limit})

        return AfterIDPaginatedList(
            Station,
            self._requester,
            self.endpoint + "stations/",
            first_id=after_id,
            **kwargs
        )

    def get_status(self, **kwargs):
        """Returns the status of all stations inside this group.

        If this group contains groups, returns the status of all stations within
        the groups within this group. Recursively.

        :rtype: :class:`GroupStatus`
        """

        status = self._requester.request_data(
            "GET", self.endpoint + "status/", parameters=kwargs
        )
        return GroupStatus(
            status["offline_count"], status["powered_on"], status["in_use_count"]
        )

    def get_child_groups(self, **kwargs):
        """Gets a list of Groups which are children of this Group

        :returns: list of :class:`Group`
        """

        groups = self._requester.request_data(
            "GET", self.endpoint + "groups/", parameters=kwargs
        )
        return [Group(self._requester, group_json) for group_json in groups]


GroupStatus = namedtuple("GroupStatus", ["offline", "powered_on", "in_use"])
"""Represents the status of Stations in a Group.

.. attribute:: offline

    Count of the stations which are have not checked in for a while, for example
    because they are off or asleep.

.. attribute:: powered_on

    Count of the stations which are checking in but not in use.

.. attribute:: in_use

    Count of the stations which are checking in and have an active user logged
    in.
"""


class Station(LabStatsObject):
    """Represents a computer Station."""

    id: int  #: Unique identifier

    name: str  #: Human-readable machine name. Not necessarily the hostname!

    description: str  #: Optional user-defined description

    group_id: int  #: Group which this Station belongs to

    serial_number: str  #: The machine's detected serial number

    manufacturer: str  #: The machine's detected manufacturer

    model: str  #: The machine's detected model

    host_name: str
    """Network hostname.

    Not necessarily the same as `name`. May be a FQDN or not depending on
    the client's operating system or configuration.
    """

    mac_addresses: List[str]  #: List of MAC addresses used by this station

    ip_addresses: List[str]  #: List of IPv4 addresses currently granted

    subnets: List[str]  #: List of IPv4 subnet masks for ip_addresses

    ip_v6_addresses: List[str]  #: List of IPv6 addresses currently granted

    operating_systems: List[dict]
    """List of operating systems the machine has reported with.

    An operating system dict looks like this::

        {
            "name": "windows",
            "version": "10.0.177653"
        }

    A Station may have one or more operating systems, but the only supported
    ones are ``"windows"`` and ``"macos"``.
    """

    client_version: str  #: Version of LabStats client

    form_factor: str  #: ``"Desktop"`` or ``"Laptop"``

    allow_student_routing: bool
    """Whether the LabFind app should allow users to navigate to this lab."""

    @property
    def endpoint(self):
        """The LabStats REST API endpoint that represents this object"""
        return "stations/{:d}/".format(self.id)

    def get_apps(self, limit=300, after_id=1, **kwargs):
        """Gets all the apps installed on this station.

        :param limit: See :ref:`limit`

        :param after_id: See :ref:`after_id`

        :returns: list of :class:`Application`

        :calls: ``/stations/{id}/apps``
        """

        kwargs.update({"limit": limit})

        return AfterIDPaginatedList(
            Application,
            self._requester,
            "{}apps/".format(self.endpoint),
            first_id=after_id,
            **kwargs
        )

    def get_metadata(self, **kwargs):
        """Gets a list of :class:`StationMetadataAttribute` for this Station.

        :rtype: list of :class:`StationMetadataAttribute`

        :calls: ``/stations/{id}/metadata``
        """

        metadata = self._requester.request_data(
            "GET", self.endpoint + "metadata/", parameters=kwargs
        )

        attributes = []
        for attribute in metadata["metadata_attributes"]:
            new_attribute = StationMetadataAttribute(
                attribute["name"], attribute["value"], attribute["is_user_defined"]
            )
            attributes.append(new_attribute)

        return attributes

    def get_status(self, **kwargs):
        """Gets this station's status, whether it's on and in use.

        :rtype: :class:`StationStatus`

        :calls: ``/stations/{id}/status``
        """

        status = self._requester.request_data(
            "GET", self.endpoint + "status/", parameters=kwargs
        )

        return StationStatus(status)


StationMetadataAttribute = namedtuple(
    "StationMetadataAttribute", ["name", "value", "is_user_defined"]
)
"""Represents a single attribute of extra or user-defined metadata on a Station.

.. attribute:: name

    The name of the attribute, such as ``"HardDisk_TotalBytes"``.

.. attribute:: value

    The value contained by the attribute, such as ``"250 GB"``.

.. attribute:: is_user_defined

    A boolean value for whether this attribute was imported by the user or
    created by the LabStats client.
"""


class StationStatus(Enum):
    """Represents whether a station is powered on and in use"""

    powered_on = "powered_on"
    """The station is turned on and has no active user"""

    in_use = "in_use"
    """The station is turned on and a user is actively using it"""

    offline = "offline"
    """The station is turned off or is otherwise not checking in to LabStats"""


class StationTagGroup:
    """Represents a group of tags which may be applied to a Station"""

    def __init__(self, requester, name):
        self._requester = requester
        self._name = name

    @property
    def endpoint(self):
        """The endpoint which may be used to get this tag group"""

        return "stations/tags_groups/{}/".format(self.name)

    @property
    def name(self):
        """Unique name. Also the unique identifier for the tag group."""

        return self._name

    @property
    def tags(self):
        """The tags which this tag group contains

        :calls: ``/stations/tags_groups/{tag_group}/tags``
        """

        return self._requester.request_data("GET", self.endpoint + "tags/")

    def __repr__(self):
        return "StationTagGroup(_requester={}, name={})".format(
            self._requester, self.name
        )
