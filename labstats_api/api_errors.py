"""Generic errors, or errors that should not be module-local"""


class LabStatsAPIError(Exception):
    """Base exception for LabStats API.

    Thrown in this regular form when no more specific error applies."""


class UnauthorizedError(LabStatsAPIError):
    """Thrown by LabStats when the given API key is invalid or has insufficient permissions.

    May also be thrown if the IP you are accessing the API from is not whitelisted for
    the key.
    """


class RequestInvalidError(LabStatsAPIError):
    """Thrown by LabStats when the request is invalid"""


class NotFoundError(LabStatsAPIError):
    """Thrown by LabStats when the requested item was not found"""
