"""The LabStats API "Starter Session". Start here to get anywhere in the API"""

from requests import Session

from .api_errors import LabStatsAPIError
from .requester import Requester
from .paginated_list import AfterIDPaginatedList
from .models import Application, Group, Station, StationTagGroup


class KeyNotGivenError(LabStatsAPIError):
    """Thrown when no API key is given"""


class URLNotGivenError(LabStatsAPIError):
    """Thrown when no API URL is given"""


class LabStats:
    """A LabStats API session.

    The LabStats object is the "starter" object for using LabStats API. Most API
    operations will start from here.

    See the `LabStats REST API Documentation <https://api.labstats.com>`_ for more
    information on REST API calls. See :doc:`/usage` for information on using
    LabStats API.

    :param api_url: URL of your LabStats server's API. If you have a hosted instance,
                    see `API Documentation and Testing
                    <https://support.labstats.com/article/api/#api-documentation-testing>`_.
                    Otherwise, your API URL will be specific to your self-hosted
                    instance.

    :param api_key: API authorization key to query the LabStats server with. See
                    `API Key Creation
                    <https://support.labstats.com/article/api/#api-key-creation>`_ for
                    more information.

    :param session: A Requests Session object to use for this instance. See `Advanced
                    Usage -- Requests
                    <https://2.python-requests.org/en/master/user/advanced/>`_ for more
                    information. If you're unsure if you need this, you probably don't.
                    If this option is omitted (or is ``None``), a Session will be
                    created for you.
    """

    def __init__(self, api_url, api_key, session=None):

        if not session:
            session = Session()

        self._requester = Requester(api_url, api_key, session)

    def get_app(self, application, **kwargs):
        """Gets a single ``[Application]`` from a LabStats account

        :param application: The application to query for.

        :type application: :class:`labstats_api.models.Application` or int

        :returns: :class:`labstats_api.models.Application`

        :calls: ``/apps/{id}/``
        """

        if isinstance(application, Application):
            application = application.id

        response = self._requester.request_data(
            "GET", "apps/{}/".format(application), parameters=kwargs
        )
        return Application(self._requester, response)

    def get_apps(
        self, type=None, search=None, limit=300, after_id=1, **kwargs
    ):  # pylint: disable=redefined-builtin
        """Gets a list of all the Applications from a LabStats account

        :param type: The type of application to receive. Valid values are ``"catalog"``,
                     ``"inventory"``, or ``"user_defined"``.

        :param search: A search term used to filter applications

        :type search: str

        :param limit: See :ref:`limit`

        :param after_id: See :ref:`after_id`

        :returns: list of :class:`labstats_api.models.Application`

        :calls: ``/apps/``

        Additional parameters to the request can be passed as ``kwargs``
        """

        kwargs.update({"type": type, "search": search, "limit": limit})

        return AfterIDPaginatedList(
            Application, self._requester, "apps/", first_id=after_id, **kwargs
        )

    def get_app_tags(self, **kwargs):
        """Gets a list of all of the application tags from a LabStats account

        :returns: list of ``str``

        :calls: ``/apps/tags/``
        """

        return self._requester.request_data("GET", "apps/tags/", parameters=kwargs)

    def get_groups(
        self,
        search=None,
        contents=None,
        has_lab_features_enabled=None,
        capability=None,
        **kwargs,
    ):
        """Gets a list of all the Groups from a LabStats account

        :param search: Search term

        :param contents: ``"stations"`` or ``"groups"``

        :param has_lab_features_enabled: Whether lab features are enabled or not

        :param capability: Search for a single lab capability

        :returns: list of :class:`.models.Group`

        :calls: ``/groups/``
        """

        kwargs.update(
            {
                "search": search,
                "contents": contents,
                "has_lab_features_enabled": has_lab_features_enabled,
                "capability": capability,
            }
        )

        groups = self._requester.request_data("GET", "groups/", parameters=kwargs)

        return [Group(self._requester, group_json) for group_json in groups]

    def get_group(self, group, **kwargs):
        """ Gets a single Group from a LabStats account

        :param group: Group to query for

        :type group: :class:`.models.Group` or int

        :returns: :class:`.models.Group`

        :calls: ``/groups/{id}/``
        """

        if isinstance(group, Group):
            group = group.id

        response = self._requester.request_data(
            "GET", "groups/{:d}".format(group), parameters=kwargs
        )
        return Group(self._requester, response)

    def get_stations(  # pylint: disable=invalid-name,too-many-arguments
        self,
        status=None,
        os=None,
        form_factor=None,
        has_application=None,
        limit=200,
        after_id=1,
        **kwargs,
    ):
        """Gets a list of all the Stations from a LabStats account.

        :param status: Filter based on the state of the machine. Valid values
                       are ``"powered_on"``, ``"offline"``, ``"in_use"``, or
                       ``None``.

        :param os: Filter based on the machine's operating system. Valid values
                   are ``"windows"``, ``"macos"``, or ``None``.

        :param form_factor: Filter based on the machine's form factor. Valid
                            values are ``"desktop"`` or ``"laptop"``.

        :param has_application: Filter based on whether the machine has this
                                   application installed.

        :type has_application: int or :class:`.models.Application`

        :param limit: See :ref:`limit`

        :param after_id: See :ref:`after_id`
        """

        if isinstance(has_application, Application):
            has_application = has_application.id

        kwargs.update(
            {
                "status": status,
                "os": os,
                "form_factor": form_factor,
                "has_application_id": has_application,
                "limit": limit,
            }
        )

        return AfterIDPaginatedList(
            Station, self._requester, "stations/", first_id=after_id, **kwargs
        )

    def get_station(self, station, **kwargs):
        """Gets a single Station from a LabStats account

        :param group: Group to query for

        :type group: :class:`.models.Group` or int

        :returns: :class:`.models.Group`

        :calls: ``/stations/{id}/``
        """

        if isinstance(station, Station):
            station = station.id

        response = self._requester.request_data(
            "GET", "stations/{:d}".format(station), parameters=kwargs
        )
        return Station(self._requester, response)

    def get_station_tag_groups(self, **kwargs):
        """Gets a list of Station Tag groups which organize Tags for Stations.

        :rtype: List of :class:`.models.StationTagGroup`

        :calls: ``/stations/tag_groups``
        """

        tag_groups = self._requester.request_data(
            "GET", "stations/tag_groups/", parameters=kwargs
        )

        return [StationTagGroup(self._requester, name) for name in tag_groups]

    def get_station_tags(self, **kwargs):
        """Gets all of the Station Tags which organize Stations

        :rtype: List of str

        :calls: ``/stations/tags``
        """

        return self._requester.request_data("GET", "stations/tags/", parameters=kwargs)

    def get_stations_with_tag(self, tag, limit=200, after_id=1, **kwargs):
        """Gets all of the stations that have the given tag."""

        kwargs.update({"limit": limit})

        return AfterIDPaginatedList(
            Station,
            self._requester,
            "stations/tags/{}/stations/".format(tag),
            first_id=after_id,
            **kwargs,
        )
