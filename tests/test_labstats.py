"""Tests for the labstats_api.LabStats central object"""

# pylint: disable=protected-access,redefined-outer-name

import pytest

from common import assert_test_parameter_is_included
from labstats_api import LabStats
from labstats_api.models import Application, Station
from mocks.fake_requester import FakeRequester


@pytest.fixture
def labstats_with_mock():
    """A LabStats object with a FakeRequester set as its requester"""

    labstats = LabStats(api_url="https://0.0.0.0", api_key="fake_key")
    labstats._requester = FakeRequester()
    return labstats


def test_get_app_int(labstats_with_mock):
    """Ensure that get_app calls the API correctly if given an integer"""

    app = labstats_with_mock.get_app(1000, test="yes")

    assert isinstance(app, Application)
    assert app.name == "Microsoft Word"
    assert_test_parameter_is_included(labstats_with_mock)


def test_get_app_app(labstats_with_mock):
    """Ensure that get_app calls the API correctly if given an Application"""

    app_check = Application(None, {"id": 1001})

    app = labstats_with_mock.get_app(app_check, test="yes")

    assert isinstance(app, Application)
    assert app.name == "Microsoft Excel"
    assert_test_parameter_is_included(labstats_with_mock)


def test_get_apps(labstats_with_mock):
    """Ensure that get_apps gets a valid PaginatedList"""

    result = labstats_with_mock.get_apps(
        type="test_type", search="test_search", limit=1, after_id=1000, test="yes"
    )

    assert result[0].name == "Microsoft Internet Explorer"
    assert result[5].name == "Wordpad"
    assert_test_parameter_is_included(
        labstats_with_mock, {"search": "test_search", "limit": 1}
    )


def test_get_apps_tags(labstats_with_mock):
    """Ensure that you can get app tags"""

    result = labstats_with_mock.get_app_tags(test="yes")

    assert result[0] == "3D graphics"
    assert result[13] == "FTP client"
    assert_test_parameter_is_included(labstats_with_mock)


def test_get_groups(labstats_with_mock):
    """Ensures that get_groups returns a valid PaginatedList"""

    groups = labstats_with_mock.get_groups(
        search="test_search",
        contents="test",
        has_lab_features_enabled=False,
        capability="test_cap",
        test="yes",
    )

    assert groups[0].name == "Child"
    assert groups[5].name == "Parent"
    assert_test_parameter_is_included(
        labstats_with_mock,
        {
            "search": "test_search",
            "contents": "test",
            "has_lab_features_enabled": False,
            "capability": "test_cap",
        },
    )


def test_get_group(labstats_with_mock):
    """Ensures you can get a single group"""

    group = labstats_with_mock.get_group(1011, test="yes")

    assert group.name == "Child"
    assert group.contents == "stations"
    assert_test_parameter_is_included(labstats_with_mock)


def test_get_stations(labstats_with_mock):
    """Ensures you can get all stations"""

    args = {
        "status": "test_off",
        "os": "test_os",
        "form_factor": "briefcase",
        "has_application": 101,
        "limit": 1,
        "test": "yes",
    }

    expected_args = {
        "status": "test_off",
        "os": "test_os",
        "form_factor": "briefcase",
        "has_application_id": 101,
        "limit": 1,
        "test": "yes",
    }

    stations = labstats_with_mock.get_stations(**args)

    assert stations[0].id == 1001
    assert stations[1].ip_addresses[1] == "182.118.119.107"
    assert stations[2].name == "test-mac-2"
    assert_test_parameter_is_included(labstats_with_mock, extra_args=expected_args)


def test_get_stations_with_application(labstats_with_mock):
    """Ensures an Application can be used to filter Stations"""

    application = Application(None, {"id": 1001})

    stations = labstats_with_mock.get_stations(
        after_id=1000, has_application=application, test="yes"
    )
    __ = stations[0]

    assert_test_parameter_is_included(
        labstats_with_mock, extra_args={"has_application_id": 1001}
    )


def test_get_station(labstats_with_mock):
    """Ensures a single Station can be retrieved"""

    station = labstats_with_mock.get_station(1005, test="yes")

    assert isinstance(station, Station)
    assert station.id == 1005
    assert_test_parameter_is_included(labstats_with_mock)


def test_get_station_with_station(labstats_with_mock):
    """Ensures a single Station can be retrieved using a Station"""

    mock_station = Station(None, {"id": 1005})

    station = labstats_with_mock.get_station(mock_station, test="yes")

    assert isinstance(station, Station)
    assert station.id == 1005
    assert_test_parameter_is_included(labstats_with_mock)


def test_get_station_tag_groups(labstats_with_mock):
    """Ensures you can get station tag groups"""

    tag_groups = labstats_with_mock.get_station_tag_groups(test="yes")

    assert tag_groups[0].name == "Cool"
    assert tag_groups[1].name == "Uncool"
    assert tag_groups[2].name == "Broken"
    assert_test_parameter_is_included(labstats_with_mock)


# This test remains in test_labstats because it is the *only* test for
# StationTagGroup. Once there are more, create a new file.
def test_station_tag_group(labstats_with_mock):
    """Ensures a Station Tag Group can get its child tags"""

    tag_group = labstats_with_mock.get_station_tag_groups()[0]

    assert tag_group.tags == ["15C", "60F"]


def test_station_tags(labstats_with_mock):
    """Ensures you can get station tags"""

    tags = labstats_with_mock.get_station_tags(test="yes")

    assert tags == ["15C", "60F", "Great", "neat", "☠️🦹"]
    assert_test_parameter_is_included(labstats_with_mock)


def test_get_stations_with_tag(labstats_with_mock):
    """Ensures you can get stations using a tag"""

    stations = labstats_with_mock.get_stations_with_tag("neat", test="yes")

    assert stations[0].name == "test-windows-2"
    assert stations[1].name == "test-windows-3"
    assert_test_parameter_is_included(labstats_with_mock)
