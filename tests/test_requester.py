"""Tests for the abstraction of direct communication with the LabStats API"""

import json
from unittest import mock

import pytest

from labstats_api.api_errors import (
    LabStatsAPIError,
    NotFoundError,
    RequestInvalidError,
    UnauthorizedError,
)
from labstats_api.requester import Requester


class FakeSession:
    """A fake Session object which returns fake responses

    :param status: A numeric status code to return (example 200, 404)

    :param content: Text to set as the response's ``.text`` parameter
                    (UTF-8, probably JSON)
    """

    def __init__(self, status, content):
        self.get = mock.Mock(return_value=FakeResponse(status, content))


class FakeResponse:
    """A fake requests Response object with only the data needed to make the
    Requester happy
    """

    def __init__(self, status_code, content):
        self.status_code = status_code
        self.text = content

    def json(self):
        """Converts self.text to JSON"""
        return json.loads(self.text)


def create_requester(mock_session):
    """Creates a test requester with mock_session"""
    return Requester("https://0.0.0.0", "test_key", mock_session)


def test_basic_request():
    """Ensures the Requester tries to authorize itself correctly and passes URLs"""

    mock_session = FakeSession(200, "null")

    requester = create_requester(mock_session)
    requester.request_data(
        "GET", "test/", parameters={"hi": "hello"}, headers={"header": "yes"}
    )
    mock_session.get.assert_called_with(
        "https://0.0.0.0/test/",
        params={"hi": "hello"},
        headers={"Authorization": "test_key", "header": "yes"},
        timeout=10,
    )


def test_default_headers():
    """Ensures the default authorization header works"""
    mock_session = FakeSession(200, "null")

    requester = create_requester(mock_session)
    requester.request_data("GET", "test/")
    mock_session.get.assert_called_with(
        "https://0.0.0.0/test/",
        params=None,
        headers={"Authorization": "test_key"},
        timeout=10,
    )


def test_401():
    """Ensures the Requester handles 401 errors"""

    mock_session = FakeSession(401, "null")

    requester = create_requester(mock_session)
    with pytest.raises(UnauthorizedError):
        requester.request_data("GET", "test/")


def test_400():
    """Ensures the Requester handles 400 errors"""

    mock_session = FakeSession(400, "null")

    requester = create_requester(mock_session)
    with pytest.raises(RequestInvalidError):
        requester.request_data("GET", "test/")


def test_404():
    """Ensures the Requester handles 404 errors"""

    mock_session = FakeSession(404, "null")

    requester = create_requester(mock_session)
    with pytest.raises(NotFoundError):
        requester.request_data("GET", "test/")


def test_other_error():
    """Ensures the Requester has a catch-all for non-200 requests."""

    mock_session = FakeSession(999, "null")

    requester = create_requester(mock_session)
    with pytest.raises(LabStatsAPIError):
        requester.request_data("GET", "test/")


def test_post():
    """POST is not implemented, it should raise an error."""

    requester = create_requester(None)
    with pytest.raises(NotImplementedError):
        requester.request_data("POST", "test/")
