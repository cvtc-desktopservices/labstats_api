"""Common assertions used for multiple files

This module's main use is to avoid reaching into private API as much as possible
in most tests, making it easier to refactor private API without needing to edit
a ton of tests
"""

# pylint: disable=protected-access


def assert_test_parameter_is_included(ls_object, extra_args=None):
    """
    Ensures that {"test": "yes"} and any given extra arguments are included in
    the object's requester's last call parameters.

    To use this assertion set, make your call with ``test=yes`` anywhere that
    ``**kwargs`` are accepted, which should be everywhere. Make sure that the
    ``ls_object`` you're passing has a FakeRequester as its requester.

    :param ls_object: Anything which holds its requester in an ``_requester``
                      attribute

    :param extra_args: More arguments to assert were passed through
    """

    # Ensure that request_data has actually been called
    assert ls_object._requester.request_data.call_args

    parameters = ls_object._requester.request_data.call_args[1]["parameters"]

    assert "test" in parameters.keys()
    assert parameters["test"] == "yes"

    for key, value in (extra_args or {}).items():
        assert key in parameters.keys()
        assert parameters[key] == value
