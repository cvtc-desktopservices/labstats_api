"""Tests for the abstraction of the LabStats pagination"""

# pylint: disable=protected-access,redefined-outer-name

import pytest

from labstats_api.models import Application
from labstats_api.paginated_list import AfterIDPaginatedList
from mocks.fake_requester import FakeRequester

MOCKED_REQUESTER = FakeRequester()


def grow_list_to_maximum(grow_list):
    """Exhausts all the items in a PaginatedList so it has loaded everything"""
    [__ for __ in grow_list]  # pylint: disable=pointless-statement,unnecessary-comprehension


@pytest.fixture()
def empty_application_list():
    """Creates an AfterIDPaginatedList which would create Applications, but has
    no data to get
    """
    new = AfterIDPaginatedList(
        Application, MOCKED_REQUESTER, request_endpoint="apps/", first_id=1012
    )
    grow_list_to_maximum(new)
    return new


@pytest.fixture()
def full_application_list():
    """Creates an AfterID PaginatedList which has Applications in it"""
    new = AfterIDPaginatedList(
        Application, MOCKED_REQUESTER, request_endpoint="apps/", first_id=1000
    )
    grow_list_to_maximum(new)
    return new


def test_empty_application_data(empty_application_list):
    """Ensure an expected empty list does not accidentally create objects"""
    assert not empty_application_list._elements


def test_full_application_data(full_application_list):
    """Ensure the list gets all of the elements we expect"""
    assert len(full_application_list._elements) == 6


def test_application_slice(full_application_list):
    """Ensure the list can create slices which work as we expect"""
    new_slice = full_application_list[1:4]
    name_list = [app.name for app in new_slice]
    assert name_list[0] == "Mozilla Firefox"
    assert name_list[1] == "Google Chrome"
    assert name_list[2] == "Notepad++"


def test_negative_index():
    """Ensure negative indexes get items from the end of the list"""

    new = AfterIDPaginatedList(
        Application, MOCKED_REQUESTER, request_endpoint="apps/", first_id=1000
    )

    with pytest.raises(IndexError):
        new[-1]  # pylint: disable=pointless-statement


def test_negative_index_slice_start():
    """Ensure negative indexes get items from the end of the list"""

    new = AfterIDPaginatedList(
        Application, MOCKED_REQUESTER, request_endpoint="apps/", first_id=1000
    )

    with pytest.raises(IndexError):
        new[-1:1]  # pylint: disable=pointless-statement


def test_negative_index_slice_end():
    """Ensure negative indexes get items from the end of the list"""

    new = AfterIDPaginatedList(
        Application, MOCKED_REQUESTER, request_endpoint="apps/", first_id=1000
    )

    with pytest.raises(IndexError):
        new[1:-1]  # pylint: disable=pointless-statement
