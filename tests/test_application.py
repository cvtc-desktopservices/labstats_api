"""Tests for the Application and its family of models"""

# pylint: disable=redefined-outer-name

import json

import pytest

from labstats_api.models import Application
from mocks.fake_requester import FakeRequester
from mocks.fake_requester_data import single_app_data
from common import assert_test_parameter_is_included


@pytest.fixture
def app_with_mock():
    """Creates an Application with a FakeRequester"""

    return Application(FakeRequester(), json.loads(single_app_data()))


def test_get_versions(app_with_mock):
    """Ensures get_versions returns a valid list of ApplicationVersion

    Also ensures it passes parameters correctly
    """

    versions = app_with_mock.get_versions(test="yes")

    assert versions[0].version == "16.0.10730.20304"
    assert versions[1].version == "16.0.10730.20344"
    assert_test_parameter_is_included(app_with_mock)
