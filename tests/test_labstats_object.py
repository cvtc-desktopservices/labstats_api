"""Tests for the LabStatsObject base model"""

# pylint: disable=no-member,redefined-outer-name

import json
from datetime import datetime

import pytest

from labstats_api.models import LabStatsObject


@pytest.fixture
def test_json():
    """JSON that can create a small Application-like LabStatsObject"""
    return json.loads(
        """{
        "id": 1000,
        "name": "Microsoft Word",
        "description": null,
        "is_tracked": true,
        "tracking_pattern_match": "wildcard"
    }"""
    )


def test_attribute_setting(test_json):
    """Ensure that the object can set its own attributes correctly"""
    thing = LabStatsObject(None, test_json)

    assert thing.id == 1000
    assert thing.name == "Microsoft Word"
    assert thing.description is None
    assert thing.is_tracked
    assert thing.tracking_pattern_match == "wildcard"


def test_date_setting():
    """Ensure that setting Date-type attributes works correctly"""

    expected_date = datetime(2019, 7, 23, 17, 25, 34, 47000)
    expected_datestring = "2019-07-23T17:25:34.047"
    thing = LabStatsObject(None, json.loads('{"date": "' + expected_datestring + '"}'))
    assert thing.date == expected_datestring
    assert thing.date_date == expected_date


def test_json_continuity(test_json):
    """Ensure that JSON in is the same as JSON out if no changes are made to the object"""

    thing = LabStatsObject(None, test_json)

    assert thing.name == "Microsoft Word"
    assert thing.to_json() == json.dumps(test_json)
