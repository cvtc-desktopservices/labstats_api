"""Mocks the Requester module of LabStats API"""

import json
from unittest.mock import Mock

from . import fake_requester_data


class FakeRequester:
    """A mock of labstats_api's requester object

    Returns data as needed by the tests.
    """

    def __init__(self):
        self.request_data = Mock(wraps=self._request_data)

    @classmethod
    def strip_endpoint(cls, endpoint):
        """Strips everything but the ID from an endpoint such as ``apps/1000/``
        or ``apps/1000/versions/``

        For example, returns ``"1000"`` for ``apps/1000/`` or
        ``apps/1000/versions``
        """
        return endpoint.split("/", 1)[-1].split("/")[0]

    @classmethod
    def _request_data(  # pylint: disable=too-many-return-statements,too-many-branches
        cls, method: str, endpoint: str, parameters: dict = None
    ) -> dict:
        """Returns plausible fake data depending on the parameters given"""

        if method == "GET":
            if endpoint.startswith("apps"):
                if endpoint == "apps/":
                    return json.loads(
                        fake_requester_data.app_data(parameters.get("after_id"))
                    )
                if endpoint == "apps/tags/":
                    return json.loads(fake_requester_data.app_tag_data())

                app_id = int(cls.strip_endpoint(endpoint))

                if endpoint.endswith("versions/"):
                    # apps/{id}/versions
                    return json.loads(fake_requester_data.app_versions_data(app_id))

                # probably a apps/{id}/ request
                return json.loads(fake_requester_data.single_app_data(app_id))

            if endpoint.startswith("groups"):
                if endpoint == "groups/":
                    return json.loads(fake_requester_data.groups_data())

                group_id = int(cls.strip_endpoint(endpoint))

                if endpoint.endswith("stations/"):
                    # groups/{id}/stations
                    # Luckily, this endpoint returns the same data as /stations
                    # As long as we aren't doing more in-depth testing, we can
                    # just use that.
                    return json.loads(
                        fake_requester_data.stations_data(parameters.get("after_id"))
                    )

                if endpoint.endswith("groups/"):
                    # groups/{id}/groups/
                    return json.loads(fake_requester_data.group_in_group_data(group_id))

                if endpoint.endswith("status/"):
                    # groups/{id}/status/
                    return json.loads(fake_requester_data.group_status_data(group_id))

                return json.loads(fake_requester_data.group_data(group_id))

            if endpoint.startswith("stations"):
                if endpoint == "stations/":
                    return json.loads(
                        fake_requester_data.stations_data(parameters.get("after_id"))
                    )

                if endpoint == "stations/tag_groups/":
                    return json.loads(fake_requester_data.station_tag_groups_data())

                if endpoint.startswith("stations/tags_groups/"):
                    # /stations/tags_groups/{tag_group}/tags
                    # This is a convenient typo, it makes matching this easier
                    return json.loads(
                        fake_requester_data.station_tag_groups_children_data()
                    )

                if endpoint == "stations/tags/":
                    return json.loads(fake_requester_data.station_tags_data())

                if endpoint.startswith("stations/tags/"):
                    # stations/tags/{tag}/stations
                    tag = endpoint.split("stations/tags/")[-1].split("/stations/")[0]
                    print(tag)
                    return json.loads(
                        fake_requester_data.station_tags_stations_data(tag=tag)
                    )

                station_id = int(cls.strip_endpoint(endpoint))

                if endpoint.endswith("metadata/"):
                    # stations/{id}/metadata/
                    return json.loads(
                        fake_requester_data.station_metadata_data(station_id=station_id)
                    )

                if endpoint.endswith("status/"):
                    # stations/{id}/status/
                    return json.loads(
                        fake_requester_data.station_status_data(station_id=station_id)
                    )

                if endpoint.endswith("apps/"):
                    # stations/{id}/apps/
                    # It's the same model as the /apps/ endpoint
                    return json.loads(fake_requester_data.app_data())

                # stations/{id}
                return json.loads(
                    fake_requester_data.station_data(station_id=station_id)
                )

        raise NotImplementedError("Didn't hit any known endpoints")
