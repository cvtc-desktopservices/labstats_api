"""Contains data used to feed :class:`labstats_api.test.mocks.fake_requester.FakeRequester`"""


def single_app_data(app_id=1000):
    """Returns a JSON object string for a single application."""

    if app_id == 1000:
        return """{
            "$type": "Application",
            "id": 1000,
            "name": "Microsoft Word",
            "vendor": "Microsoft Corporation",
            "source": "catalog",
            "description": null,
            "type": "desktop",
            "is_tracked": true,
            "tracking_pattern_match": "wildcard"
        }"""
    if app_id == 1001:
        return """{
            "id": 1001,
            "name": "Microsoft Excel",
            "vendor": "Microsoft Corporation",
            "source": "catalog",
            "description": null,
            "type": "desktop",
            "is_tracked": true,
            "tracking_pattern_match": "wildcard"
        }"""
    if app_id == 1002:
        return """{
            "id": 1002,
            "name": "Microsoft PowerPoint",
            "vendor": "Microsoft Corporation",
            "source": "catalog",
            "description": null,
            "type": "desktop",
            "is_tracked": true,
            "tracking_pattern_match": "wildcard"
        }"""

    raise NotImplementedError()


def app_data(after_id=1000):
    """Gets some fake application data.

    Three sets of data are available to represent 6 different applications.
    These applications start at id 1005 and go to ID 1011. ID 1010 is skipped. 5 are
    returned if after_id is less than 1009. 1 more is returned for 1010 or 1011. A blank
    "no more data" entry is returned for all other requests.
    """
    if after_id < 1009:
        return """{
            "results": [
                {
                    "$type": "Application",
                    "id": 1005,
                    "name": "Microsoft Internet Explorer",
                    "vendor": "Microsoft Corporation",
                    "source": "catalog",
                    "description": null,
                    "type": "desktop",
                    "is_tracked": true,
                    "tracking_pattern_match": "wildcard"
                },
                {
                    "$type": "Application",
                    "id": 1006,
                    "name": "Mozilla Firefox",
                    "vendor": "Mozilla Foundation",
                    "source": "catalog",
                    "description": null,
                    "type": "desktop",
                    "is_tracked": true,
                    "tracking_pattern_match": "wildcard"
                },
                {
                    "$type": "Application",
                    "id": 1007,
                    "name": "Google Chrome",
                    "vendor": "Google Inc.",
                    "source": "catalog",
                    "description": null,
                    "type": "desktop",
                    "is_tracked": true,
                    "tracking_pattern_match": "wildcard"
                },
                {
                    "$type": "Application",
                    "id": 1008,
                    "name": "Notepad++",
                    "vendor": "Don HO don.h@free.fr",
                    "source": "catalog",
                    "description": null,
                    "type": "desktop",
                    "is_tracked": true,
                    "tracking_pattern_match": "wildcard"
                },
                {
                    "$type": "Application",
                    "id": 1009,
                    "name": "Notepad",
                    "vendor": "Microsoft Corporation",
                    "source": "catalog",
                    "description": null,
                    "type": "desktop",
                    "is_tracked": true,
                    "tracking_pattern_match": "reg_ex"
                }
            ],
            "max_page_size": 300,
            "additional_pages": true,
            "last_id": 1009
        }"""

    if after_id < 1011:
        return """{
            "results": [
                {
                    "$type": "Application",
                    "id": 1011,
                    "name": "Wordpad",
                    "vendor": "Microsoft Corporation",
                    "source": "catalog",
                    "description": null,
                    "type": "desktop",
                    "is_tracked": true,
                    "tracking_pattern_match": "reg_ex"
                }
            ],
            "max_page_size": 300,
            "additional_pages": false,
            "last_id": 1011
        }"""

    return """{
        "results": [],
        "max_page_size": 300,
        "additional_pages": false,
        "last_id": null
    }"""


def app_tag_data():
    """Some sample data, like ``/apps/tags/`` returns"""
    return """[
        "3D graphics",
        "3D modeling",
        "adobe",
        "animation",
        "apple",
        "audio editing",
        "autodesk",
        "browser",
        "building information modeling",
        "CAD",
        "data analysis",
        "education",
        "email",
        "FTP client",
        "google",
        "google docs",
        "image editing"
    ]"""


def app_versions_data(app_id):
    """Plausible data that ``/apps/{id}/versions`` would return"""
    if app_id == 1000:
        return """{
            "results": [
                {
                    "$type": "ApplicationVersion",
                    "id": 854,
                    "name": "Microsoft Word",
                    "version": "16.0.10730.20304",
                    "operating_system": "windows",
                    "install_count": 1,
                    "application_id": 1000
                },
                {
                    "$type": "ApplicationVersion",
                    "id": 1415,
                    "name": "Microsoft Word",
                    "version": "16.0.10730.20344",
                    "operating_system": "windows",
                    "install_count": 1,
                    "application_id": 1000
                }
            ]
        }"""

    raise NotImplementedError


def groups_data():
    """Returns plausible output from the ``/groups`` endpoint"""
    return """[
        {
            "id": 1011,
            "parent_group_id": 1004,
            "contents": "stations",
            "name": "Child",
            "description": null,
            "schedule_id": null,
            "has_lab_features_enabled": false
        },
        {
            "id": 1012,
            "parent_group_id": 1004,
            "contents": "stations",
            "name": "004",
            "description": null,
            "schedule_id": null,
            "has_lab_features_enabled": false
        },
        {
            "id": 1013,
            "parent_group_id": 1004,
            "contents": "stations",
            "name": "006",
            "description": null,
            "schedule_id": null,
            "has_lab_features_enabled": false
        },
        {
            "id": 1014,
            "parent_group_id": 1004,
            "contents": "stations",
            "name": "007",
            "description": null,
            "schedule_id": null,
            "has_lab_features_enabled": false
        },
        {
            "id": 1015,
            "parent_group_id": 1004,
            "contents": "stations",
            "name": "008",
            "description": null,
            "schedule_id": null,
            "has_lab_features_enabled": false
        },
        {
            "id": 1004,
            "parent_group_id": null,
            "contents": "groups",
            "name": "Parent",
            "description": null,
            "schedule_id": null,
            "has_lab_features_enabled": false
        }
    ]"""


def group_data(group_id):
    """Returns plausible output from the ``/groups/{id}`` endpoint"""

    if group_id == 1004:
        return """{
            "id": 1004,
            "parent_group_id": null,
            "contents": "groups",
            "name": "Parent",
            "description": null,
            "schedule_id": null,
            "has_lab_features_enabled": false
        }"""

    if group_id == 1011:
        return """{
            "id": 1011,
            "parent_group_id": 1004,
            "contents": "stations",
            "name": "Child",
            "description": null,
            "schedule_id": null,
            "has_lab_features_enabled": false
        }"""

    raise NotImplementedError("Don't have a group with that ID")


def group_in_group_data(group_id):
    """Returns plausible output from the ``/groups/{id}/groups`` endpoint"""

    if group_id == 1004:
        return """[
            {
                "id": 1011,
                "parent_group_id": 1004,
                "contents": "stations",
                "name": "Child",
                "description": null,
                "schedule_id": null,
                "has_lab_features_enabled": false
            },
            {
                "id": 2000,
                "parent_group_id": 1004,
                "contents": "stations",
                "name": "Child 2",
                "description": "Another child group",
                "schedule_id": null,
                "has_lab_features_enabled": false
            }
        ]"""

    raise NotImplementedError("Don't have a parent group with that ID")


def group_status_data(group_id):
    """Returns plausible output from the ``/groups/{id}/status`` endpoint"""

    if group_id == 1004:
        return """{
            "offline_count": 58,
            "powered_on": 7,
            "in_use_count": 1
        }"""

    raise NotImplementedError("Don't have group status data for that group")


def stations_data(after_station_id=1000):
    """Returns plausible data from the ``/stations`` endpoint"""

    if after_station_id < 1001:
        return """{
            "results": [
                {
                    "$type": "Station",
                    "id": 1001,
                    "name": "test-mac-1",
                    "description": null,
                    "group_id": null,
                    "serial_number": "GHITXOIV",
                    "manufacturer": "Apple",
                    "model": "Macmini8,1",
                    "host_name": "test-mac-1.local",
                    "mac_addresses": [
                        "1d:7d:00:9d:73:5f"
                    ],
                    "ip_addresses": [
                        "162.67.61.16"
                    ],
                    "subnets": [
                        "255.255.254.0"
                    ],
                    "ip_v6_addresses": [
                        ""
                    ],
                    "operating_systems": [
                        {
                            "name": "macos",
                            "version": "10.14.6"
                        }
                    ],
                    "client_version": "6.19.513.3",
                    "form_factor": "Desktop",
                    "allow_student_routing": true
                },
                {
                    "$type": "Station",
                    "id": 1002,
                    "name": "test-windows-1",
                    "description": null,
                    "group_id": 1011,
                    "serial_number": "TGAJDLVI",
                    "manufacturer": "Hewlett-Packard",
                    "model": "HP EliteDesk 800 G1 DM",
                    "host_name": "test-windows-1.local",
                    "mac_addresses": [
                        "92:5a:79:96:6a:6b",
                        "E7:93:42:D6:84:91"
                    ],
                    "ip_addresses": [
                        "155.239.160.49",
                        "182.118.119.107"
                    ],
                    "subnets": [
                        "255.255.254.0",
                        "255.0.0.0"
                    ],
                    "ip_v6_addresses": [
                        "5095:671b:be5f:8ed4:1714:1764:6c3b:25d7"
                    ],
                    "operating_systems": [
                        {
                            "name": "windows",
                            "version": "10.0.17763"
                        }
                    ],
                    "client_version": "6.19.110.4",
                    "form_factor": "Desktop",
                    "allow_student_routing": true
                }
            ],
            "max_page_size": 200,
            "additional_pages": true,
            "last_id": 1002
        }"""
    if after_station_id == 1002:
        return """{
            "results": [
                {
                    "$type": "Station",
                    "id": 1003,
                    "name": "test-mac-2",
                    "description": null,
                    "group_id": 2000,
                    "serial_number": "AKZSMWTE",
                    "manufacturer": "Apple",
                    "model": "iMac14,3",
                    "host_name": "test-mac-2.local",
                    "mac_addresses": [
                        "a2:08:92:05:61:4a"
                    ],
                    "ip_addresses": [
                        "164.52.0.156"
                    ],
                    "subnets": [
                        "255.255.254.0"
                    ],
                    "ip_v6_addresses": [
                        ""
                    ],
                    "operating_systems": [
                        {
                            "name": "macos",
                            "version": "10.14.3"
                        },
                        {
                            "name": "windows",
                            "version": "10.0.17763"
                        }
                    ],
                    "client_version": "6.19.110.4",
                    "form_factor": "Desktop",
                    "allow_student_routing": true
                },
                {
                    "$type": "Station",
                    "id": 1005,
                    "name": "test-windows-2",
                    "description": null,
                    "group_id": 1011,
                    "serial_number": "EZDMYPDO",
                    "manufacturer": "Dell",
                    "model": "Latitude E7440",
                    "host_name": "test-windows-2.local",
                    "mac_addresses": [
                        "c2:2f:50:86:93:f3"
                    ],
                    "ip_addresses": [
                        "195.172.48.72"
                    ],
                    "subnets": [
                        "255.255.254.0"
                    ],
                    "ip_v6_addresses": [
                        ""
                    ],
                    "operating_systems": [
                        {
                            "name": "windows",
                            "version": "10.0.17763"
                        }
                    ],
                    "client_version": "6.19.110.4",
                    "form_factor": "Desktop",
                    "allow_student_routing": true
                }
            ],
            "max_page_size": 200,
            "additional_pages": false,
            "last_id": 1005
        }"""

    raise NotImplementedError("Don't have a response for that station after_id")


def station_data(station_id=1005):
    """Returns plausible data from the ``/stations/{id}`` endpoint"""

    if station_id == 1005:
        return """{
            "$type": "Station",
            "id": 1005,
            "name": "test-windows-2",
            "description": null,
            "group_id": 1011,
            "serial_number": "EZDMYPDO",
            "manufacturer": "Dell",
            "model": "Latitude E7440",
            "host_name": "test-windows-2.local",
            "mac_addresses": [
                "c2:2f:50:86:93:f3"
            ],
            "ip_addresses": [
                "195.172.48.72"
            ],
            "subnets": [
                "255.255.254.0"
            ],
            "ip_v6_addresses": [
                ""
            ],
            "operating_systems": [
                {
                    "name": "windows",
                    "version": "10.0.17763"
                }
            ],
            "client_version": "6.19.110.4",
            "form_factor": "Desktop",
            "allow_student_routing": true
        }"""

    raise NotImplementedError("Don't know that station")


def station_metadata_data(station_id=1005):
    """Returns plausible data for the ``/stations/{id}/metadata`` endpoint"""

    if station_id == 1005:
        return """{
            "station_id": 1005,
            "metadata_attributes": [
                {
                    "name": "is_cool",
                    "value": "yes",
                    "is_user_defined": true
                },
                {
                    "name": "HardDisk_AvailableBytes",
                    "value": "169.0 GB",
                    "is_user_defined": false
                },
                {
                    "name": "HardDisk_TotalBytes",
                    "value": "233 GB",
                    "is_user_defined": false
                }
            ]
        }"""

    raise NotImplementedError("Don't have metadata for that station")


def station_status_data(station_id=1005):
    """Returns plausible data for the ``/stations/{id}/status`` endpoint"""

    if station_id == 1001:
        return '"offline"'
    if station_id == 1002:
        return '"in_use"'
    if station_id == 1005:
        return '"powered_on"'

    raise NotImplementedError("Don't have status data for that station")


def station_tag_groups_data():
    """Returns plausible ``/stations/tag_groups`` data"""

    return """[
        "Cool",
        "Uncool",
        "Broken"
    ]"""


def station_tag_groups_children_data(tag_group="Cool"):
    """Returns plausible ``/stations/tags_groups/{tag_group}/tags`` data"""

    if tag_group == "Cool":
        return """[
            "15C",
            "60F"
        ]"""

    raise NotImplementedError("Don't have tag group children for that tag group.")


def station_tags_data():
    """Returns plausible ``/stations/tags`` data"""

    return """[
        "15C",
        "60F",
        "Great",
        "neat",
        "☠️🦹"
    ]"""


def station_tags_stations_data(tag="neat"):
    """Returns plausible ``/stations/tags/{tag}/stations`` data. Only 'neat'
    is implemented.
    """

    if tag == "neat":
        return """{
            "results": [
                {
                    "$type": "Station",
                    "id": 1005,
                    "name": "test-windows-2",
                    "description": null,
                    "group_id": 1011,
                    "serial_number": "EZDMYPDO",
                    "manufacturer": "Dell",
                    "model": "Latitude E7440",
                    "host_name": "test-windows-2.local",
                    "mac_addresses": [
                        "c2:2f:50:86:93:f3"
                    ],
                    "ip_addresses": [
                        "195.172.48.72"
                    ],
                    "subnets": [
                        "255.255.254.0"
                    ],
                    "ip_v6_addresses": [
                        ""
                    ],
                    "operating_systems": [
                        {
                            "name": "windows",
                            "version": "10.0.17763"
                        }
                    ],
                    "client_version": "6.19.110.4",
                    "form_factor": "Desktop",
                    "allow_student_routing": true
                },
                {
                    "$type": "Station",
                    "id": 1006,
                    "name": "test-windows-3",
                    "description": null,
                    "group_id": 1011,
                    "serial_number": "EZDEYPDO",
                    "manufacturer": "Dell",
                    "model": "Latitude E7440",
                    "host_name": "test-windows-3.local",
                    "mac_addresses": [
                        "c2:2f:50:86:54:f3"
                    ],
                    "ip_addresses": [
                        "195.172.48.73"
                    ],
                    "subnets": [
                        "255.255.254.0"
                    ],
                    "ip_v6_addresses": [
                        ""
                    ],
                    "operating_systems": [
                        {
                            "name": "windows",
                            "version": "10.0.17763"
                        }
                    ],
                    "client_version": "6.19.110.4",
                    "form_factor": "Desktop",
                    "allow_student_routing": true
                }
            ],
            "max_page_size": 200,
            "additional_pages": false,
            "last_id": 1006
        }"""

    raise NotImplementedError("No data for stations with that tag")
