"""Tests for the Group and its family of models"""

# pylint: disable=redefined-outer-name

import json

import pytest

from labstats_api.models import Group
from mocks.fake_requester import FakeRequester
from mocks.fake_requester_data import group_data
from common import assert_test_parameter_is_included


@pytest.fixture
def group_with_mock():
    """Creates a Group with a FakeRequester"""

    return Group(FakeRequester(), json.loads(group_data(1004)))


def test_group_stations(group_with_mock):
    """Ensures you can get the stations that belong to a group"""

    args = {"status": "test_status", "limit": 1, "test": "yes"}

    stations = group_with_mock.get_stations(**args)

    assert stations[0].id == 1001
    assert stations[1].ip_addresses[1] == "182.118.119.107"
    assert stations[2].name == "test-mac-2"
    assert_test_parameter_is_included(group_with_mock, extra_args=args)


def test_group_in_group(group_with_mock):
    """Ensures you can get the groups that belong to a group

    Also ensures it passes parameters correctly
    """

    children = group_with_mock.get_child_groups(test="yes")

    assert isinstance(children[0], Group)
    assert children[0].id == 1011
    assert children[1].id == 2000

    assert_test_parameter_is_included(group_with_mock)


def test_group_status(group_with_mock):
    """Ensures you can get group.status"""

    status = group_with_mock.get_status(test="yes")

    assert status.offline == 58
    assert status.powered_on == 7
    assert status.in_use == 1

    assert_test_parameter_is_included(group_with_mock)
