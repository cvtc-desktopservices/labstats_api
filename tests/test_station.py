"""Tests for the Station and its family of models"""

# pylint: disable=redefined-outer-name

import json

import pytest

from labstats_api.models import Station, StationStatus
from mocks.fake_requester import FakeRequester
from mocks.fake_requester_data import station_data
from common import assert_test_parameter_is_included


@pytest.fixture
def station_with_mock():
    """Creates a Station with a FakeRequester"""

    return Station(FakeRequester(), json.loads(station_data(1005)))


def test_get_metadata(station_with_mock):
    """Ensures that you can get metadata for a station"""

    metadata = station_with_mock.get_metadata(test="yes")

    assert metadata[0].name == "is_cool"
    assert metadata[0].value == "yes"

    assert_test_parameter_is_included(station_with_mock)


def test_get_status(station_with_mock):
    """Ensures you can get station status"""

    status = station_with_mock.get_status(test="yes")

    assert status == StationStatus.powered_on
    assert_test_parameter_is_included(station_with_mock)


def test_get_apps(station_with_mock):
    """Ensures you can get apps installed on a station"""

    apps = station_with_mock.get_apps(test="yes")

    assert apps[0].name == "Microsoft Internet Explorer"
    assert apps[1].name == "Mozilla Firefox"
    assert_test_parameter_is_included(station_with_mock)
